package main

import (
	"gitlab.com/frankvanmeurs/musical-parakeet/day01"
	"go.uber.org/zap"
)

var logger *zap.Logger

func main() {
	var err error
	logger, err = zap.NewProduction(zap.WithCaller(false))
	if err != nil {
		panic(err)
	}
	zap.ReplaceGlobals(logger)

	defer func(logger *zap.Logger) {
		_ = logger.Sync()
	}(logger)

	err = day01.Solve()
	if err != nil {
		logger.Error("Error while solving daily problem", zap.Error(err))
	}
}
