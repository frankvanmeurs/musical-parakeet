package day01

import "testing"

func BenchmarkSolutionOne(b *testing.B) {
	data, err := ReadInput("input")
	if err != nil {
		b.Error(err)
	}
	for i := 0; i < b.N; i++ {
		_, _ = solvePartOne(data, 2020)
	}
}
func BenchmarkSolutionTwo(b *testing.B) {
	data, err := ReadInput("input")
	if err != nil {
		b.Error(err)
	}
	for i := 0; i < b.N; i++ {
		_, _ = solvePartTwo(data, 2020)
	}
}
