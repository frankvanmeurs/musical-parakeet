package day01

import (
	"bufio"
	"os"
	"sort"
	"strconv"

	"go.uber.org/zap"
)

func Solve() error {
	data, err := ReadInput("day01/input")
	if err != nil {
		return err
	}
	solutionPartOne, err := solvePartOne(data, 2020)
	if err != nil {
		return err
	}
	zap.L().Info("Solution", zap.Int("Day", 1), zap.Int("Part", 1), zap.Int("Result", solutionPartOne))
	solutionPartTwo, err := solvePartTwo(data, 2020)
	if err != nil {
		return err
	}
	zap.L().Info("Solution", zap.Int("Day", 1), zap.Int("Part", 2), zap.Int("Result", solutionPartTwo))
	return nil
}

// solvePartOne Solves the problem of finding to integers who's sum is 2020 in a list and multiplying them
func solvePartOne(data []int, sum int) (int, error) {

	// Sort the list, go from the back
	sort.Ints(data)

	for i := len(data) - 1; i > 0; i-- {
		t := sum - data[i]
		needle := sort.Search(
			i-1, func(j int) bool {
				return data[j] >= t
			},
		)
		if needle > 0 && needle < i && data[i]+data[needle] == sum {
			return data[i] * data[needle], nil
		}
	}

	return 0, nil
}
func solvePartTwo(data []int, sum int) (int, error) {

	// Sort the list, go from the back
	sort.Ints(data)

	for i := len(data) - 1; i > 0; i-- {
		// Take the value at i and deduct it from 2020 to get a new target value
		t1 := sum - data[i]
		res, err := solvePartOne(data[0:i], t1)
		if err != nil {
			return 0, err
		}
		if res == 0 {
			continue
		}
		return res * data[i], nil
	}
	return 0, nil
}

func ReadInput(fname string) ([]int, error) {

	fptr, err := os.Open(fname)
	if err != nil {
		return nil, err
	}
	defer func() {
		if err := fptr.Close(); err != nil {
			zap.L().Fatal("Error closing file", zap.Error(err))
		}
	}()

	s := bufio.NewScanner(fptr)
	s.Split(bufio.ScanLines)

	var data []int
	for s.Scan() {
		aDatum := s.Text()
		iDatum, err := strconv.Atoi(aDatum)
		if err != nil {
			return data, err
		}
		data = append(data, iDatum)
	}
	return data, nil
}
